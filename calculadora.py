#!/usr/bin/python3

def plus(m, n):
    return m + n


def minus(m, n):
    return m - n


# MAIN EN PYTHON
if __name__ == "__main__":
    print("The result of summing 1 and 2 is ", plus(1, 2))
    print("The result of summing 3 and 4 is ", plus(3, 4))
    print("The result of resting 5 to 6 is ", minus(6, 5))
    print("The result of resting 7 to 8 is ", minus(8, 7))
